'use strict'

const uploadImgForm = document.querySelector('.upload-image'),
    rangeInput = document.querySelector('.quality'),
    qualityValueEl = document.querySelector('.quality-value')

let USER_ID; // will contain user id


const showConvertedFile = (blob, filename) => {
    const imageUrl = URL.createObjectURL(blob);
    const imageContainer = document.querySelector('.result');

    const imageElement = document.createElement('img'),
        imageWrapper = document.createElement('div');
    imageElement.style.width = '250px';
    imageElement.style.height = '150px';
    imageElement.style.objectFit = 'contain';
    imageElement.src = imageUrl;

    imageWrapper.classList.add('result__item')

    const downloadLink = document.createElement('a');
    downloadLink.href = imageUrl;
    downloadLink.download = filename;
    downloadLink.textContent = 'Скачать';

    imageWrapper.appendChild(imageElement);
    imageWrapper.appendChild(downloadLink);

    imageContainer.prepend(imageWrapper)
}

const askAboutFile = () => {
    // спрашиваем обработался ли файл
    if (!USER_ID) { // АЙДИ ЕСТЬ?
        console.warn('Нет ID у пользователя')
        return
    }
    fetch(`http://localhost:3000/getProcessedFile:${USER_ID}`)
    .then(response => {
        if (response.status === 202) {
            // Обработка статуса 202 Accepted
            throw new Error(`The file hasn't yet been converted`);
        } else {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
        }

        console.log('ТОЧНО ПОЛУЧИЛИ ФАЙЛ!')
        // Нужно удалить сетИнтервал, если мы больше не спрашиваем
        const filenameHeader = response.headers.get('Content-Disposition');
        const filenameMatch = filenameHeader && filenameHeader.match(/filename='(.+)'/);

        let filename = 'newImage';
        if (filenameMatch && filenameMatch[1]) {
            filename = filenameMatch[1];
        }

        return response.blob().then(blob => [blob, filename]);
    })
    .then(([blob, filename]) => {
        showConvertedFile(blob, filename);

        // delete setInterval
        removeInterval('waitFile')
    })
    .catch(error => {
        console.error('Error during ask about uploaded file:', error);
    });
}

// Создаем объект-контейнер для интервалов
const intervalContainer = {};

// Функция для добавления интервала в контейнер
const addInterval = (name, callback, interval) => {
  // Проверяем, что интервал с таким именем еще не существует
  if (!intervalContainer[name]) {
    // Создаем новый интервал и сохраняем его ссылку в контейнере
    intervalContainer[name] = setInterval(callback, interval);
  }
};

// Функция для удаления интервала по имени
const removeInterval = (name) => {
  // Проверяем, что интервал с таким именем существует
  if (intervalContainer[name]) {
    // Очищаем интервал и удаляем его из контейнера
    clearInterval(intervalContainer[name]);
    delete intervalContainer[name];
  }
}

const getId = async () => {
    return fetch('http://localhost:3000/getUserId')
    .then(response => {
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        return response.json()
    })
    .then(data => data.userId)
}

const uploadImg = async () => {
    if (!USER_ID) { // АЙДИ ЕСТЬ?
        USER_ID = await getId()
    }
    const formData = new FormData(uploadImgForm);
    formData.append('userId', USER_ID);
    // Тут нужно чтобы тоже отправлялся ID
    fetch('http://localhost:3000/upload', {
        method: 'POST',
        body: formData
    })
    .then(response => {
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        } else {
            console.log('Отправили файл на обработку')
        }
    })
    .catch(error => {
        console.error('Error during upload:', error);
    });
}


uploadImgForm.addEventListener('submit', e => {
    console.log('uploadImgForm submit!')
    e.preventDefault();
    uploadImg();

    if (!intervalContainer['waitFile']) {
        addInterval('waitFile', askAboutFile, 1000);
    }
})

const updateRangeInputValue = (input, showValueElement) => {
    const updater = () => {
        showValueElement.textContent = input.value + '%'
    }
    updater();
    input.addEventListener('input', updater);
    input.addEventListener('change', updater);
}

updateRangeInputValue(rangeInput, qualityValueEl)
