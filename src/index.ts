import './pre-start'; // Must be the first import
import logger from 'jet-logger';

import EnvVars from '@src/constants/EnvVars';
import server from './server';

// **** Run **** //
// start main logic
import sharp from 'sharp';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import { uuid } from 'uuidv4';

interface ImageRequestBody {
  userId: string;
  quality: string;
}
interface FileObject {
  name: string,
  path: string,
  quality: number;
  userId: string;
}


const filesQueue: 
  { name: string; path: string; quality: number; userId: string; }[]  = [],
  maxProcessingCount = 3,
  processedFiles: { name: string; file: Buffer; userId: string; }[] = [];
  

// удаление файлов из /uploads после обработки
const deleteFile = (filePath: string) => {
  fs.unlink(filePath, err => {
    if (err) {
      console.error(err);
    } else {
      console.log('File is deleted.');
    }
  });
};

const convertImage = async (
  name: string, path: string, imageQuality: number, userId: string,
): Promise<
{ name: string; file: Buffer; userId: string; }
> => {
  // Сжатие и конвертация в формат WebP
  const convertedBuffer = await sharp(path)
    .toFormat('webp', { quality: imageQuality ?? 80 }) // default quality = 80
    .toBuffer();

  const newFile = {
    name: `${name.split('.')[0]}.webp`,
    file: convertedBuffer,
    userId: userId,
  };

  deleteFile(path);

  return newFile;
};

const processFiles = async () => { 
  const sleep = (ms: number) => {
    return new Promise(res => setTimeout(res, ms));
  };

  if (filesQueue.length === 0) {
    // Если файлов на обработку нет, "заснуть" на некоторый период
    await sleep(1500);
    await processFiles();
  } else {
    const currentBatch = filesQueue.splice(0, maxProcessingCount);

    const processedBatch: { name: string; file: Buffer; userId: string; }[] = 
      await Promise.all(currentBatch.map(async (file) => {
      // Вместо этой части вызывайте вашу функцию обработки
      
        const processedData = await convertImage((file as FileObject)?.name,
          (file as FileObject)?.path,
          (file as FileObject)?.quality,
          (file as FileObject)?.userId);
      
        return processedData;
      }));

    // Обработанные файлы добавляются в массив processedFiles
    processedFiles.push(...processedBatch);

    // Рекурсивный вызов для обработки следующего пакета файлов
    await processFiles();
  }
};

// Настройка хранения загруженных файлов
const rootDir = path.resolve(__dirname, '..');
const uploadDir: string = path.join(rootDir, 'uploads');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadDir);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

// Обработка загрузки файлов
server.post('/upload', upload.single('image'), (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).send('No file uploaded.');
    }

    const { path, originalname } = req.file,
      imageQuality: number = Number((req.body as ImageRequestBody)?.quality),
      userId: string = (req.body as ImageRequestBody)?.userId;

    const fileObj = {
      name: originalname,
      path: path,
      quality: imageQuality,
      userId: userId,
    };

    filesQueue.push(fileObj);

  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
});

// Маршрут для получения уникального ID
server.get('/getUserId', (req, res) => {
  const userId = uuid();

  res.send({ userId });
});

const updateProcessedFiles = (fileId: string) => {
  const updatedProcessedFiles = 
    processedFiles.filter(file => file.userId !== fileId);
  processedFiles.splice(0, processedFiles.length);
  processedFiles.push(...updatedProcessedFiles);
};

// Получение обработанного файла по уникальному идентификатору
server.get('/getProcessedFile:fileId', (req, res) => {

  const fileId = req.params.fileId.split(':')[1];
  let actualFile;
  if (processedFiles.length) {
    actualFile = processedFiles.find(file => file.userId === fileId);

    if (actualFile) {
      // теперь отправим файл нужному юзеру?
      // Отправка результата клиенту
      res.set('Content-Type', 'image/webp');
      res.set(
        'Content-Disposition',
        `attachment; filename='${actualFile.name.split('.')[0]}.webp'`,
      );
      res.send(actualFile.file);

      // update array processedFiles[]
      updateProcessedFiles(fileId);
    }
  } else {
    res.status(202).json({
      status: 'Accepted',
      message: 'File accepted for processing. Check back later for the result.',
    });
  }
});

processFiles();
// end main logic

const SERVER_START_MSG =
  'Express server started on port: ' + EnvVars.Port.toString();

server.listen(EnvVars.Port, () => logger.info(SERVER_START_MSG));
